(function ($){


    var firstheight = (mobilecheck()) ? window.innerHeight : 480,
        firstwidth = (mobilecheck()) ? window.innerWidth : 600,
        game = new Phaser.Game(firstwidth, firstheight, Phaser.AUTO, 'game_div'),
        bestscore = localStorage.getItem('bestscore') || 0; 
             

    function mobilecheck () {
      var check = false;
      (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
    }


    // Initialize Phaser, and create a 400x490px game
    //  var game = new Phaser.Game(width, height, Phaser.AUTO, 'gameDiv');

    // Create our 'main' state that will contain the game
    var mainState = {

        preload: function() { 

            // Change the background color of the game
            game.stage.backgroundColor = '#71c5cf';

            // Load the bird sprite
            game.load.spritesheet('bird', 'images/bird.png', 46, 32); 

            // Load pipe sprite 
            game.load.spritesheet('pipe', 'images/pipes2.png', 54, 320);

            // load ground image 
            game.load.image('ground', 'images/ground.png');

            // load background image 
            game.load.image('background', 'images/background.png');  

            // load title image 
            game.load.image('title', 'images/flappy.png');  
            game.load.image('tap', 'images/tap.png');  

            // load scoreboard 
            game.load.image('scoreboard', 'images/scoreboard.png');  
            game.load.image('gameover', 'images/gameover.png');  
            game.load.image('newbest', 'images/new.png');  
            game.load.image('ok', 'images/ok.png');  

            // sounds
            this.load.audio('score', 'sounds/point.mp3');
            this.load.audio('flap', 'sounds/wing.mp3');
            this.load.audio('die', 'sounds/die.mp3');
            this.load.audio('hit', 'sounds/hit.mp3');

            // background music 
            this.load.audio('bgmusic', 'sounds/background.mp3')

        },

        create: function() { 
            // Set the physics system
            game.physics.startSystem(Phaser.Physics.ARCADE);

            this.firstpipe = true; 
            this.vertpipes = []; 
            this.allpipes = this.game.add.group(); 

            this.hasScored = false; 
            this.index = 0;  
            this.pipeindex = 0; 

            this.backgroundMusic = this.game.add.audio('bgmusic');
            this.backgroundMusic.volume = 0.5;
            this.backgroundMusic.loop = true;
            this.backgroundMusic.play();

            this.collision = true;     

            // sounds     
            this.scoreSound = this.game.add.audio('score');
            this.flapSound = this.game.add.audio('flap');
            this.hitSound = this.game.add.audio('hit'); 
            this.dieSound = this.game.add.audio('die');    

            this.flapSound.volume = 0.5; 
            this.scoreSound.volume = 0.3; 

            // add background image 
            this.background = this.game.add.tileSprite(0, this.game.height - (228 / 2) - 112, this.game.width, 228, 'background');
            this.background.autoScroll(-30, 0);

            // add ground tiles 
            this.ground = this.game.add.tileSprite(0, this.game.height - (112 / 2), this.game.width, 112, 'ground');
            this.ground.autoScroll(-100, 0);

            // instruction images  
            this.title = this.game.add.image(this.game.width / 2, 100,'title'); 
            this.tap = this.game.add.image(this.title.x + 40,this.title.y + 100,'tap'); 
            this.title.anchor.setTo(0.5, 0.5);
            this.tap.anchor.setTo(0.5, 0.5);

            this.game.physics.arcade.enableBody(this.ground);
            this.ground.allowGravity = false;
            this.ground.body.immovable = true;  

            // Display the bird on the screen
            this.bird = this.game.add.sprite(this.game.width / 2 - 70, this.tap.y , 'bird');
            this.bird.anchor.setTo(0.5, 0.5);  
            this.bird.allowGravity = false;
            this.bird.alive = false;
            this.introtween = this.game.add.tween(this.bird).to({y:this.tap.y + 20}, 450, Phaser.Easing.Linear.NONE, true, 0, 1000, true);  

            //animate the bird
            this.bird.animations.add("fly", [0,1,2]); 
            this.bird.animations.play("fly", 10, true);

            // Add gravity to the bird to make it fall
            game.physics.arcade.enable(this.bird); 


            // Should we generate pipes 
            this.genpipes = true; 

            // Call the 'jump' function when the spacekey is hit
            this.spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            this.input.onDown.add(this.jump, this);  
            this.input.onDown.addOnce(this.startGame, this);
            
            this.levelGroup = this.game.add.group();
            

            // score    
            this.score = 0;  
            
        },

        startGame: function() {  
            this.bird.alive = true;
            this.newbest = false; 
            this.bird.allowGravity = true;
            this.bird.body.gravity.y = 1000; 
            this.game.tweens.remove(this.introtween);

            // add a timer
            this.pipeGenerator = game.time.events.loop(Phaser.Timer.SECOND * 1.25, this.generatePipes, this); 
            this.pipeGenerator.timer.start();

            // destroy intro stuff 
            this.tap.destroy(); 
            this.title.destroy(); 

            // set score
            this.labelScore = game.add.text(this.game.width / 2, 20, "0", { font: "30px flappyfont", fill: "#ffffff" });
            this.labelScore.anchor.setTo(0.5, 0.5);   

        },

        update: function() {
           // If the bird is out of the world (too high or too low), call the 'restartGame' function

            if (this.bird.angle < 90 && this.bird.alive == true) {
                this.bird.angle += 1.5;
            } 
                

            if(this.collision) {
                if (this.bird.inWorld == false) {
                    this.hitSound.play();
                    this.restartGame();
                }

                this.game.physics.arcade.collide(this.ground, this.bird, this.scoreBoard, null, this); 

                for (var x = 0; x < this.vertpipes.length; x++) {

                    this.game.physics.arcade.collide(this.bird, this.vertpipes[x], function(){
                        for (var x = 0; x < this.vertpipes.length; x++) {
                            this.vertpipes[x].body.velocity.x = 0;    
                        } 
                        this.hitSound.play();
                         
                        this.scoreBoard();
                    }.bind(this), null, this);
                };
            }
            

            this.checkScore();

            //game.physics.arcade.overlap(this.bird, this.ground, this.scoreBoard);  
        }, 


        // Make the bird jump 
        jump : function() {  
            if(this.bird.alive) {
                this.flapSound.play();

                this.bird.animations.play("fly", 1, false);
                // Add a vertical velocity to the bird
                this.bird.body.velocity.y = -360;

                // Create an animation on the bird
                var animation = game.add.tween(this.bird);

                // Set the animation to change the angle of the sprite to -20° in 100 milliseconds
                animation.to({angle: -20}, 100);

                // And start the animation
                animation.start();  
            }
            
        },


        setLocalScore : function () {
            localStorage.setItem('bestscore', this.score);
            this.newbest = true; 
            return this.score; 
        }, 

        scoreBoard : function () {
            
            this.freezeMovement(); 
            //this.spaceKey.onDown.add(this.restartGame, this); 
            
            bestscore = (this.score > bestscore) ? this.setLocalScore() : bestscore; 
            
            score = (this.score == 0) ? "0" : this.score; 
            
            this.scoreboardgroup = this.game.add.group();

            this.gameover = this.game.add.image(this.game.width / 2, 100, 'gameover'); 
            this.gameover.anchor.setTo(0.5, 0.5);
            this.gameover.alpha = 0;

            this.ok =  game.add.button(this.game.width / 2, this.game.height + 300, 'ok', this.playState);
            this.ok.anchor.setTo(0.5, 0.5);
            this.ok.events.onInputDown.add(function () {
                this.restartGame(); 
            }.bind(this), this, 2);
            

            this.scoreboard = this.game.add.image(this.game.width / 2, this.game.height + 200, 'scoreboard'); 
            this.scoreboard.anchor.setTo(0.5, 0.5);
            


            this.scoretext = this.game.add.text(this.scoreboard.x + this.scoreboard.width / 2 - 40, this.scoreboard.y - 25, score, { font: "20px flappyfont", fill: "#000" });
            this.bestscoretext = this.game.add.text(this.scoreboard.x + this.scoreboard.width / 2 - 40, this.scoreboard.y + 20, bestscore, { font: "20px flappyfont", fill: "#000" });

            this.scoreboardgroup.add(this.scoreboard); 
            this.scoreboardgroup.add(this.scoretext); 
            this.scoreboardgroup.add(this.bestscoretext); 
            this.scoreboardgroup.add(this.ok); 

            if(this.newbest) {
                this.newbestimg = this.game.add.image(this.scoreboard.width - 50, this.game.height + 200, 'newbest'); 
                this.scoreboardgroup.add(this.newbestimg); 
            }

            var t = game.add.tween(this.gameover).to( { alpha: 1 }, 800, Phaser.Easing.Linear.None, true, true);
            t.onComplete.add(function () {
                game.add.tween(this.scoreboardgroup).to({y: -(this.game.height)}, 1000, Phaser.Easing.Bounce.Out, true);  
            }.bind(this), this);

        }, 

        freezeMovement : function () {
            this.backgroundMusic.destroy(); 
            this.labelScore.destroy(); 
            this.collision = false; 
            this.bird.body.velocity.x = 0;
            this.bird.alive = false;
            this.bird.immovable = true; 
            this.background.stopScroll();
            this.ground.stopScroll();

            if (this.pipeGroup) {
                this.genpipes = false;
            }
            this.bird.animations.stop("fly"); 
        }, 
        

        restartGame : function() {    
            game.state.start('main');
        },

        generatePipes: function() {  
            
            if (this.genpipes)
                this.PipeGroup(this.game);

            this.levelGroup.remove(this.pipeGroup); 
            this.levelGroup.add(this.pipeGroup); 
            this.levelGroup.remove(this.ground);
            this.levelGroup.add(this.ground); 

        },

        Pipe : function(game, x, y, frame) {  
          
          this.pipe = this.game.add.sprite(x, y, 'pipe', frame);  
          this.pipe.anchor.setTo(0.5, 0.5);

          this.game.physics.arcade.enableBody(this.pipe);

          this.pipe.body.allowGravity = false;
          this.pipe.body.immovable = true;

          return this.pipe; 

        },  

        PipeGroup : function(game, parent) {  
            //Phaser.Group.call(this, game, parent);
 
            this.pipeGroup = this.game.add.group();

            //y = pipe.height + (bird.height * 5) 

            this.topPipe = this.Pipe(this.game, 0, 0, 0);
            this.bottomPipe = this.Pipe(this.game, 0, 440, 1); 

            this.vertpipes[this.index] = this.topPipe; 
            this.index++; 

            this.vertpipes[this.index] = this.bottomPipe; 
            this.index++; 

            this.pipeGroup.add(this.topPipe);
            this.pipeGroup.add(this.bottomPipe);


            var bufferval = 20; 

            // toppos 
            var toppos = this.game.height - 160 - 56 - (32*5), 
                bottompos = -Math.abs(100); 

            toppos = (toppos < 0) ? 0 : toppos; 

            var pipeY = this.game.rnd.integerInRange(bottompos,toppos); 
            this.pipeGroup.x = this.game.width;
            this.pipeGroup.y = pipeY;

            this.topPipe.body.velocity.x = -200;  
            this.bottomPipe.body.velocity.x = -200;    

            this.pipeGroup.outOfBoundsKill = true;



            if (this.firstpipe)
                this.currPipe = this.vertpipes[this.pipeindex];

            if (this.hasScored) {
                this.pipeindex = this.pipeindex + 2; 
                this.currPipe = this.vertpipes[this.pipeindex];
            }
                  

            this.currPipe.alpha = 0.5; 
            this.firstpipe = false; 
            this.hasScored = false;

        }, 

        checkScore: function() {  

            if(this.currPipe && !this.hasScored && this.currPipe.world.x <= this.bird.world.x) {
                 
                this.scoreSound.play();
                this.hasScored = true; 
                this.currPipe = ""; 
                this.score++;
                this.labelScore.setText(this.score.toString());
            }
            
        }, 


    };

    // Add and start the 'main' state to start the game
    game.state.add('main', mainState);  
    game.state.start('main');  


})(jQuery); 